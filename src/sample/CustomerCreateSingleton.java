package sample;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.sql.*;



public class CustomerCreateSingleton extends Stage{
    static CustomerCreateSingleton singleton;

    VBox messagePane;
    Scene messageScene;
    Label messageLabel;

    Label lbluser;
    Label lblpassword;
    TextField usernameField;
    PasswordField passwordField;
    String username;
    String password;
    HBox usernameContainer;
    HBox passwordContainer;


    Label lblName;
    Label lblAddress;
    Label lblPhoneNumber;
    Label lblPay;

    TextField nameField;
    TextField addressField;
    TextField phoneField;
    ComboBox payField;

    String name;
    String address;
    String phone;
    String pay;

    HBox nameContainer;
    HBox addressContainer;
    HBox phoneContainer;
    HBox payContainer;

    Button yesButton;
    Button cancelButton;
    String selection;

    public static final String CREATE   = "Create";
    public static final String CANCEL = "Cancel";

    private CustomerCreateSingleton(){}

    public static CustomerCreateSingleton getSingleton(){
        if(singleton == null)
            singleton = new CustomerCreateSingleton();
        return singleton;
    }
    public void init(Stage primaryStage){
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        messageLabel = new Label();
        usernameField = new TextField();
        passwordField = new PasswordField();
        usernameContainer = new HBox();
        passwordContainer = new HBox();
        lbluser = new Label("Username: ");
        lblpassword = new Label("Password: ");

        usernameContainer.getChildren().addAll(lbluser,usernameField);
        usernameContainer.setPadding(new Insets(10,10,10,10));
        passwordContainer.getChildren().addAll(lblpassword,passwordField);
        passwordContainer.setPadding(new Insets(10,10,10,10));

        lblName = new Label("Full name: ");
        lblAddress = new Label("Full address: ");
        lblPhoneNumber = new Label("Phone number");
        lblPay = new Label("Payment Method");

        nameField = new TextField();
        addressField = new TextField();
        phoneField = new TextField();
        payField = new ComboBox();
        payField.getItems().setAll("PayPal","Credit Card","Debit Card");
        payField.setValue("PayPal");

        nameContainer = new HBox();
        nameContainer.getChildren().addAll(lblName,nameField);
        nameContainer.setPadding(new Insets(10,10,10,10));


        addressContainer = new HBox();
        addressContainer.getChildren().addAll(lblAddress,addressField);
        addressContainer.setPadding(new Insets(10,10,10,10));

        phoneContainer = new HBox();
        phoneContainer.getChildren().addAll(lblPhoneNumber,phoneField);
        phoneContainer.setPadding(new Insets(10,10,10,10));

        payContainer = new HBox();
        payContainer.getChildren().addAll(lblPay,payField);
        payContainer.setPadding(new Insets(10,10,10,10));

        yesButton = new Button(CREATE);
        cancelButton = new Button(CANCEL);

        EventHandler<ActionEvent> yesNoCancelHandler = event -> {
            CustomerCreateSingleton.this.selection = ((Button) event.getSource()).getText();
            CustomerCreateSingleton.this.hide();

            if(((Button)event.getSource()).getText().equals("Create")) {
                username = usernameField.getText();
                password = passwordField.getText();
                name = nameField.getText();
                address = addressField.getText();
                phone = phoneField.getText();
                pay = payField.getValue().toString();

                usernameField.clear();
                passwordField.clear();
                nameField.clear();
                addressField.clear();
                phoneField.clear();
                payField.setValue("PayPal");

                Connection connection = null;
                try {
                    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false","root","li970623");
                    Statement statement =  connection.createStatement();

                    try {
                        String query = "INSERT INTO Customer (Id,Password,Name,Address,PhoneNumber) VALUES (\"" + username + "\",\"" + password + "\",\"" + name + "\",\"" + address + "\",\"" + phone + "\");";
                        System.out.println(query);
                        statement.executeUpdate(query);
                        String queryc = "INSERT INTO Cart(id,CustomerId,Item1Quantity,Item2Quantity,Item3Quantity) VALUES (\""+username+"-c\",\""+username+"\","+0+","+0+","+0+");";
                        System.out.println(queryc);
                        statement.executeUpdate(queryc);
                        String queryp = "INSERT INTO Payment(PaymentId, CustomerId,PaymentType) VALUES(\""+username+"-p\",\""+username+"\",\""+pay+"\");";
                        System.out.println(queryp);
                        statement.executeUpdate(queryp);

                    }catch (MySQLIntegrityConstraintViolationException ex){
                        MessageDialogSingleton dialogSingleton = MessageDialogSingleton.getSingleton();
                        dialogSingleton.show("ERROR CREATING USER","Username is taken, please try another");
                    }
                    if(connection != null){
                        connection.close();
                        System.out.println("Connection established");
                    }else System.out.println("Connection is not established");
                }catch (Exception e){
                    e.printStackTrace();
                }





                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        };

        yesButton.setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(yesNoCancelHandler);

        HBox buttonContainer = new HBox();
        buttonContainer.getChildren().addAll(yesButton,cancelButton);

        VBox infoField = new VBox();
        infoField.getChildren().addAll(usernameContainer,passwordContainer,nameContainer,addressContainer,phoneContainer,payContainer);

        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().addAll(messageLabel,infoField,buttonContainer);
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);

    }
    public void show(String title, String message){
        setTitle(title);
        messageLabel.setText(message);
        showAndWait();
    }
}