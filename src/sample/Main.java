package sample;

import javafx.application.Application;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private Stage stage;
    private static Main instance;
    String userType;
    String userName;
    int totalPrice;
    public Main() {
        instance = this;
    }
    // static method to get instance of view
    public static Main getInstance() {
        return instance;
    }
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        stage = primaryStage;
        primaryStage.setTitle("Welcome to the CSE 305 E-Commerce Simulator");
        CustomerCreateSingleton dialog1 = CustomerCreateSingleton.getSingleton();
        dialog1.init(primaryStage);
        ProviderCreateSingleton dialog2 = ProviderCreateSingleton.getSingleton();
        dialog2.init(primaryStage);
        MessageDialogSingleton dialog3 = MessageDialogSingleton.getSingleton();
        dialog3.init(primaryStage);
        Scene mainScene = new Scene(root, 1000, 750);
        primaryStage.setScene(mainScene);

        primaryStage.show();
    }
    public void gotoMain(){
        try {
            replaceSceneContent("main.fxml");

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public void gotoCustomer(){
        try{
            replaceSceneContent("customer.fxml");
        }catch (Exception ex) {
            System.out.print("the customerfxml doesnot exist");
        }
    }
    public void gotoProvider(){
        try{
            replaceSceneContent("provider.fxml");
        }catch (Exception ex) {
            System.out.print("the providerfxml does not exist");
        }
    }
    public void gotoCart(){
        try{
            replaceSceneContent("cart.fxml");
        }catch (Exception ex){
            System.out.print("the cartfxml does not exist");
        }
    }
    public void gotoCheckOut(){
        try{
            replaceSceneContent("checkout.fxml");
        }catch (Exception ex){
            System.out.print("the checkoutfxml does not exist");
        }
    }
    public void gotoTransaction(){
        try{
            replaceSceneContent("admin.fxml");
        }catch (Exception ex){
            System.out.print("the transaction fxml does not exist");
        }
    }
    private Parent replaceSceneContent(String fxml) throws Exception {
        Parent page = (Parent) FXMLLoader.load(Main.class.getResource(fxml), null, new JavaFXBuilderFactory());
        Scene scene = stage.getScene();
        if (scene == null) {
            scene = new Scene(page, 1000, 750);

            stage.setScene(scene);
        } else {
            stage.getScene().setRoot(page);
        }
        stage.sizeToScene();
        return page;
    }
    public  Stage getStage(){
        return stage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
