package sample;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.xml.transform.Result;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class Controller {

    Connection connection;
    Statement statement;
    @FXML protected void handleCustomerSignUp(){
        CustomerCreateSingleton dialog = CustomerCreateSingleton.getSingleton();
        dialog.show("Create Customer","Please enter your info below");
    }
    @FXML protected void handleProviderSignUp(){
        ProviderCreateSingleton dialog = ProviderCreateSingleton.getSingleton();
        dialog.show("Create Provider","Please enter you info below");
    }
    @FXML protected void handleHomeRequest(){
        Main.getInstance().gotoMain();
    }

    private void handleTransactionRequest(){
        Main.getInstance().gotoTransaction();
    }

    @FXML protected void handleLoginRequest(){
        connection =null;
        ComboBox c = (ComboBox) Main.getInstance().getStage().getScene().lookup("#CorP");
        TextField usernameField = (TextField) Main.getInstance().getStage().getScene().lookup("#username");
        PasswordField passwordField = (PasswordField) Main.getInstance().getStage().getScene().lookup("#password");
        String value = (String) c.getValue();
        String username = usernameField.getText();
        String password = passwordField.getText();
        try {
            Boolean login = false;
            if(username.equals("admin")) {
                try {
                    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "root", password);
                    statement = connection.createStatement();
                    if (connection != null) {
                        login=true;
                        handleTransactionRequest();
                        TextArea allTrans = (TextArea) Main.getInstance().getStage().getScene().lookup("#allTrans");
                        TextArea allCust = (TextArea) Main.getInstance().getStage().getScene().lookup("#allCust");
                        TextArea allProv = (TextArea) Main.getInstance().getStage().getScene().lookup("#allProv");
                        Label totalTrans = (Label) Main.getInstance().getStage().getScene().lookup("#totalT");
                        int totalT = 0;
                        ResultSet Rs = statement.executeQuery("SELECT * FROM Transaction");
                        while (Rs.next()) {
                            String record = "Transaction Id: "+Rs.getString(1) + " " + Rs.getString(2) + " Item:" + Rs.getString(3) + " Amount:  " + Rs.getString(4) + " User type: " + Rs.getString(5) + " $" + Rs.getString(6);
                            totalT += Integer.parseInt(Rs.getString(6));
                            allTrans.appendText(record + "\n");
                        }
                        totalTrans.setText("Total Revenue is :$" + totalT);
                        Rs = statement.executeQuery("SELECT * FROM Customer");
                        while (Rs.next()){
                            String custRecord = "Name: "+Rs.getString(3)+" Address: "+Rs.getString(4)+" Phone Number: "+Rs.getString(5);
                            allCust.appendText(custRecord+"\n");
                        }
                        Rs = statement.executeQuery("SELECT * FROM Provider");
                        while (Rs.next()){
                            String provRecord = "Name: "+Rs.getString(4)+" Phone Number: "+Rs.getString(5);
                            allProv.appendText(provRecord+"\n");
                        }
                        connection.close();

                    }
                    } catch (SQLException e) {
                        MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
                        singleton.show("Error login","Please try again with correct username and password");
                        usernameField.clear();
                        passwordField.clear();
                        handleHomeRequest();
                    }
                }
                else if (value.equals("Customer")) {
                    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
                    statement = connection.createStatement();


                    ResultSet userRs = statement.executeQuery("Select Id FROM Customer");
                    while(userRs.next()){
                        if (username.equals(userRs.getString(1))){

                            ResultSet passwordRs = statement.executeQuery("SELECT Password FROM Customer WHERE Customer.id = \""+username+"\";");
                            passwordRs.next();
                            if(password.equals(passwordRs.getString(1))){
                                Main.getInstance().setUserName(username);
                                Main.getInstance().setUserType("Customer");
                                login = true;
                                handleCustomerRequest();
                                connection.close();
                                break;


                            }else{
                                MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
                                singleton.show("Error login","Please try again with correct username and password");
                                usernameField.clear();
                                passwordField.clear();
                                connection.close();
                                break;

                            }
                        }
                    }

                } else if (value.equals("Provider")) {
                    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "provider", "ppassword");
                    statement = connection.createStatement();
                    ResultSet userRs = statement.executeQuery("Select Id FROM Provider");
                    while(userRs.next()){
                        if (username.equals(userRs.getString(1))){

                            ResultSet passwordRs = statement.executeQuery("SELECT Password FROM Provider WHERE Provider.id = \""+username+"\";");
                            passwordRs.next();
                            if(password.equals(passwordRs.getString(1))){
                                Main.getInstance().setUserName(username);
                                Main.getInstance().setUserType("Provider");
                                handleProviderRequest();
                                login = true;
                                connection.close();
                                break;
                            }else{
                                MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
                                singleton.show("Error login","Please try again with correct username and password");
                                usernameField.clear();
                                passwordField.clear();
                                connection.close();
                                break;
                            }
                        }
                    }



                }
                if(!login){
                    MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
                    singleton.show("Error login","Please try again with correct username and password");
                    usernameField.clear();
                    passwordField.clear();
                }

        }catch (Exception ex){
            ex.printStackTrace();
        }


    }
    @FXML protected void handleCheckOutRequest(){
        String username = Main.getInstance().getUserName();
        Main.getInstance().gotoCheckOut();

        Label payment = (Label) Main.getInstance().getStage().getScene().lookup("#payment");
        Label payInstr = (Label) Main.getInstance().getStage().getScene().lookup("#payInstr");
        TextField payNum = (TextField) Main.getInstance().getStage().getScene().lookup("#payNum");
        Label payTotal = (Label) Main.getInstance().getStage().getScene().lookup("#payTotal");
        payTotal.setText("Total Price is :$"+Main.getInstance().getTotalPrice());
        payNum.setVisible(false);

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            ResultSet payRs = statement.executeQuery("SELECT * FROM Payment WHERE CustomerId = \""+username+"\";");
            payRs.next();
            if(payRs.getString("PaymentType").equals("PayPal")){
                payment.setText("Redirecting you to PayPal Now");
                payInstr.setText("You can click Pay Now to exit");
                payNum.setVisible(false);
            }else if(payRs.getString("PaymentType").equals("Credit Card")){
                payment.setText("Payment By Credit Card");
                payInstr.setText("Enter your Credit Card Number Below and Click Pay Now");
                payNum.setVisible(true);
            }else if(payRs.getString("PaymentType").equals("Debit Card")) {
                payment.setText("Payment By Debit Card");
                payInstr.setText("Enter your Credit Card Number Below and Click Pay Now");
                payNum.setVisible(true);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
    @FXML protected void handlePayNowRequest(){

        String username = Main.getInstance().getUserName();
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();

            ResultSet Rs = statement.executeQuery("SELECT * FROM Cart WHERE CustomerId = \""+username+"\";");
            Rs.next();
            int item1 = Integer.parseInt(Rs.getString("item1Quantity"));
            int item2 = Integer.parseInt(Rs.getString("item2Quantity"));
            int item3 = Integer.parseInt(Rs.getString("item3Quantity"));
            Rs= statement.executeQuery("SELECT Price FROM Item WHERE Id=1");
            Rs.next();
            int item1Price = Integer.parseInt(Rs.getString(1));

            Rs= statement.executeQuery("SELECT Price FROM Item WHERE Id=2");
            Rs.next();
            int item2Price = Integer.parseInt(Rs.getString(1));

            Rs= statement.executeQuery("SELECT Price FROM Item WHERE Id=3");
            Rs.next();
            int item3Price = Integer.parseInt(Rs.getString(1));


            if(item1 !=0){
                String uniqueId = "Trans-" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
                statement.executeUpdate("INSERT INTO Transaction VALUE(\""+uniqueId+"\",1,\"Rubber Ducky\","+item1+",\"Customer\","+(item1*item1Price)+");");
            }
            if(item2 !=0){
                String uniqueId = "Trans-" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
                statement.executeUpdate("INSERT INTO Transaction VALUE(\""+uniqueId+"\",2,\"Beach Ball\","+item2+",\"Customer\","+(item2*item2Price)+");");
            }
            if(item3 !=0){
                String uniqueId = "Trans-" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
                statement.executeUpdate("INSERT INTO Transaction VALUE(\""+uniqueId+"\",3,\"Sandals\","+item3+",\"Customer\","+(item3*item3Price)+");");
            }



            statement.executeUpdate("UPDATE Cart SET Item1Quantity=0, Item2Quantity=0,Item3Quantity=0 WHERE CustomerId = \""+username+"\";");
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        handleCustomerRequest();
    }


    @FXML protected void handleCartRequest(){
        String username = Main.getInstance().getUserName();
        Main.getInstance().gotoCart();

        Label item1Price = (Label)Main.getInstance().getStage().getScene().lookup("#item1Price");
        Label item1Amount = (Label) Main.getInstance().getStage().getScene().lookup("#item1Amount");

        Label item2Price = (Label) Main.getInstance().getStage().getScene().lookup("#item2Price");
        Label item2Amount = (Label) Main.getInstance().getStage().getScene().lookup("#item2Amount");

        Label item3Price = (Label) Main.getInstance().getStage().getScene().lookup("#item3Price");
        Label item3Amount = (Label)  Main.getInstance().getStage().getScene().lookup("#item3Amount");

        Label TotalPrice = (Label) Main.getInstance().getStage().getScene().lookup("#totalPrice");

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();

            ResultSet item1Cart = statement.executeQuery("SELECT Item1Quantity FROM Cart WHERE CustomerId = \""+username+"\";");
            item1Cart.next();
            int item1Total = Integer.parseInt(item1Cart.getString(1));
            item1Amount.setText("Amount: "+item1Cart.getString(1));
            ResultSet item1Item = statement.executeQuery("SELECT Price FROM Item WHERE id = 1");
            item1Item.next();
            item1Total = item1Total*Integer.parseInt(item1Item.getString(1));
            item1Price.setText("Total Price: $"+ item1Total);

            ResultSet item2Cart = statement.executeQuery("SELECT Item2Quantity FROM Cart WHERE CustomerId = \""+username+"\";");
            item2Cart.next();
            int item2Total = Integer.parseInt(item2Cart.getString(1));
            item2Amount.setText("Amount: "+item2Cart.getString(1));
            ResultSet item2Item = statement.executeQuery("SELECT Price FROM Item WHERE id = 2");
            item2Item.next();
            item2Total = item2Total*Integer.parseInt(item2Item.getString(1));
            item2Price.setText("Total Price: $"+ item2Total);

            ResultSet item3Cart = statement.executeQuery("SELECT Item3Quantity FROM Cart WHERE CustomerId = \""+username+"\";");
            item3Cart.next();
            int item3Total = Integer.parseInt(item3Cart.getString(1));
            item3Amount.setText("Amount: "+item3Cart.getString(1));
            ResultSet item3Item = statement.executeQuery("SELECT Price FROM Item WHERE id = 3");
            item3Item.next();
            item3Total = item3Total*Integer.parseInt(item3Item.getString(1));
            item3Price.setText("Total Price: $"+ item3Total);

            int totalPrice = item1Total+item2Total+item3Total;
            Main.getInstance().setTotalPrice(totalPrice);
            TotalPrice.setText("Total Price is:  $"+totalPrice);
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void handleCustomerRequest(){
        Main.getInstance().gotoCustomer();
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            Label item1Price = (Label) Main.getInstance().getStage().getScene().lookup("#item1Price");
            Label item2Price = (Label) Main.getInstance().getStage().getScene().lookup("#item2Price");
            Label item3Price = (Label) Main.getInstance().getStage().getScene().lookup("#item3Price");
            Label item1Stock = (Label) Main.getInstance().getStage().getScene().lookup("#item1Stock");
            Label item2Stock = (Label) Main.getInstance().getStage().getScene().lookup("#item2Stock");
            Label item3Stock = (Label) Main.getInstance().getStage().getScene().lookup("#item3Stock");

            ResultSet item1Rs = statement.executeQuery("SELECT * FROM Item WHERE id=1");
            item1Rs.next();
            item1Price.setText("Price: $"+item1Rs.getString("Price"));
            item1Stock.setText("In Stock:   "+item1Rs.getString("Quantity"));

            ResultSet item2Rs = statement.executeQuery("SELECT * FROM Item WHERE id=2");
            item2Rs.next();
            item2Price.setText("Price: $"+item2Rs.getString("Price"));
            item2Stock.setText("In Stock:   "+item2Rs.getString("Quantity"));

            ResultSet item3Rs = statement.executeQuery("SELECT * FROM Item WHERE id=3");
            item3Rs.next();
            item3Price.setText("Price: $"+item3Rs.getString("Price"));
            item3Stock.setText("In Stock:   "+item3Rs.getString("Quantity"));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    @FXML
    protected void addItem1ToCart(){

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            TextField item1Amount = (TextField) Main.getInstance().getStage().getScene().lookup("#item1Amount");
            int amount = Integer.parseInt(item1Amount.getText());
            statement.executeUpdate("UPDATE Cart SET item1Quantity = item1Quantity+"+amount+" WHERE CustomerId = \""+Main.getInstance().getUserName()+"\";");

            statement.executeUpdate("UPDATE Item Set Quantity = Quantity -"+amount+" WHERE Id = 1;");

            handleCustomerRequest();

            item1Amount.clear();
            MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
            singleton.show("Successfully adding to Cart","You have successful add the item to Cart");
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    @FXML
    protected void addItem2ToCart(){
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            TextField item2Amount = (TextField) Main.getInstance().getStage().getScene().lookup("#item2Amount");
            int amount = Integer.parseInt(item2Amount.getText());
            statement.executeUpdate("UPDATE Cart SET item2Quantity = item1Quantity+"+amount+" WHERE CustomerId = \""+Main.getInstance().getUserName()+"\";");

            statement.executeUpdate("UPDATE Item Set Quantity = Quantity -"+amount+" WHERE Id = 2;");

            handleCustomerRequest();

            item2Amount.clear();
            MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
            singleton.show("Successfully adding to Cart","You have successful add the item to Cart");
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    @FXML
    protected void addItem3ToCart(){
        try{

            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            TextField item3Amount = (TextField) Main.getInstance().getStage().getScene().lookup("#item3Amount");
            int amount = Integer.parseInt(item3Amount.getText());
            statement.executeUpdate("UPDATE Cart SET item3Quantity = item3Quantity+"+amount+" WHERE CustomerId = \""+Main.getInstance().getUserName()+"\";");

            statement.executeUpdate("UPDATE Item Set Quantity = Quantity -"+amount+" WHERE Id = 3;");

            handleCustomerRequest();

            item3Amount.clear();
            MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
            singleton.show("Successfully adding to Cart","You have successful add the item to Cart");
            connection.close();

         } catch (SQLException e) {
        e.printStackTrace();
        }

    }

    @FXML protected void removeItem1FromCart(){
        String username = Main.getInstance().getUserName();

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            ResultSet Rs = statement.executeQuery("SELECT Item1Quantity FROM Cart WHERE CustomerId = \""+username+"\";");
            Rs.next();
            statement.executeUpdate("UPDATE ITEM SET Quantity = Quantity +"+Integer.parseInt(Rs.getString(1))+" WHERE id=1;");
            statement.executeUpdate("UPDATE Cart SET Item1Quantity=0 WHERE CustomerId = \""+username+"\";");


            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        handleCartRequest();


    }
    @FXML protected void removeItem2FromCart(){
        String username = Main.getInstance().getUserName();

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            ResultSet Rs = statement.executeQuery("SELECT Item2Quantity FROM Cart WHERE CustomerId = \""+username+"\";");
            Rs.next();
            statement.executeUpdate("UPDATE ITEM SET Quantity = Quantity +"+Integer.parseInt(Rs.getString(1))+" WHERE id=2;");
            statement.executeUpdate("UPDATE Cart SET Item2Quantity=0 WHERE CustomerId = \""+username+"\";");

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        handleCartRequest();

    }
    @FXML protected void removeItem3FromCart(){
        String username = Main.getInstance().getUserName();

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            ResultSet Rs = statement.executeQuery("SELECT Item2Quantity FROM Cart WHERE CustomerId = \""+username+"\";");
            Rs.next();
            statement.executeUpdate("UPDATE ITEM SET Quantity = Quantity +"+Integer.parseInt(Rs.getString(1))+" WHERE id=3;");
            statement.executeUpdate("UPDATE Cart SET Item3Quantity=0 WHERE CustomerId = \""+username+"\";");


            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        handleCartRequest();

    }
    @FXML protected void item1Review(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            ResultSet Rs = statement.executeQuery("SELECT Review FROM Item WHERE Id=1");
            Rs.next();
            MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
            singleton.show("Item 1 Review",Rs.getString(1));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    @FXML protected void item2Review(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            ResultSet Rs = statement.executeQuery("SELECT Review FROM Item WHERE Id=2");
            Rs.next();
            MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
            singleton.show("Item 2 Review",Rs.getString(1));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }@FXML protected void item3Review(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "customer", "cpassword");
            statement = connection.createStatement();
            ResultSet Rs = statement.executeQuery("SELECT Review FROM Item WHERE Id=3");
            Rs.next();
            MessageDialogSingleton singleton = MessageDialogSingleton.getSingleton();
            singleton.show("Item 3 Review",Rs.getString(1));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
    protected void handleProviderRequest(){
        Main.getInstance().gotoProvider();
        String username = Main.getInstance().getUserName();
        String whatItem = "";
        int itemNum=0;
        Label itemType = (Label) Main.getInstance().getStage().getScene().lookup("#itemType");
        Label itemPrice = (Label) Main.getInstance().getStage().getScene().lookup("#itemPrice");
        ImageView itemImage = (ImageView) Main.getInstance().getStage().getScene().lookup("#itemImage");
        Label itemStock = (Label) Main.getInstance().getStage().getScene().lookup("#itemStock");
        Button itemFill = (Button)Main.getInstance().getStage().getScene().lookup("#itemFill");
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "provider", "ppassword");
            statement = connection.createStatement();

            ResultSet Rs = statement.executeQuery("SELECT ItemId From Provider WHERE Id=\""+username+"\";");
            Rs.next();
            if(Integer.parseInt(Rs.getString(1))==1){
                whatItem = "Rubber Ducky" ;
                itemNum=1;
                itemImage.setImage(new Image("/Image/item1.jpg"));
            }
            else if (Integer.parseInt(Rs.getString(1))==2){
                whatItem = "Beach Ball";
                itemNum=2;
                itemImage.setImage(new Image("/Image/item2.jpg"));
            }
            else if(Integer.parseInt(Rs.getString(1))==3) {
                whatItem="Sandals";
                itemNum=3;
                itemImage.setImage(new Image("/Image/item3.jpg"));
            }

            itemType.setText("You are providing "+whatItem);

            Rs = statement.executeQuery("SELECT Price From Item WHERE Name=\""+whatItem+"\";");
            Rs.next();
            itemPrice.setText("Current Price is :$"+Rs.getString(1));

            Rs = statement.executeQuery("SELECT Quantity From Item WHERE Name=\""+whatItem+"\";");
            Rs.next();
            itemStock.setText("Current Number in Stock: "+Rs.getString(1));


            final String finalWhatItem = whatItem;
            final int finalItemNum = itemNum;
            itemFill.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false", "provider", "ppassword");
                        statement = connection.createStatement();
                        statement.executeUpdate("Update Item SET Quantity = Quantity + 50 WHERE Name =\"" + finalWhatItem + "\";");

                        String uniqueId = "Trans-" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
                        statement.executeUpdate("INSERT INTO Transaction VALUE(\"" + uniqueId + "\"," + finalItemNum + ",\""+finalWhatItem+"\",50,\"Provider\",-1000);");

                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    handleProviderRequest();
                }
            });




        } catch (SQLException e) {
            e.printStackTrace();
        }



    }


}
