package sample;

/**
 * Created by xiangli on 11/25/17.
 */

import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class ProviderCreateSingleton extends Stage{
    static ProviderCreateSingleton singleton;

    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Label lbluser;
    Label lblpassword;
    TextField usernameField;
    PasswordField passwordField;
    String username;
    String password;
    HBox usernameContainer;
    HBox passwordContainer;

    Label lblName;

    Label lblPhoneNumber;

    Label lblItem;

    TextField nameField;

    TextField phoneField;

    ComboBox itemField;

    String name;

    String phone;

    int itemId;

    HBox nameContainer;
    HBox phoneContainer;
    HBox itemContainer;


    Button yesButton;
    Button cancelButton;
    String selection;

    public static final String CREATE   = "Create";
    public static final String CANCEL = "Cancel";

    private ProviderCreateSingleton(){}

    public static ProviderCreateSingleton getSingleton(){
        if(singleton == null)
            singleton = new ProviderCreateSingleton();
        return singleton;
    }
    public void init(Stage primaryStage){
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        messageLabel = new Label();
        usernameField = new TextField();
        passwordField = new PasswordField();
        usernameContainer = new HBox();
        passwordContainer = new HBox();
        lbluser = new Label("Username: ");
        lblpassword = new Label("Password: ");



        lblName = new Label("Full name: ");

        lblPhoneNumber = new Label("Phone number");

        lblItem = new Label("Which Item to Provide? Type: ");



        nameField = new TextField();
        itemField = new ComboBox();
        itemField.getItems().addAll("1","2","3");
        itemField.setValue("1");
        phoneField = new TextField();

        nameContainer = new HBox();
        nameContainer.getChildren().addAll(lblName,nameField);
        nameContainer.setPadding(new Insets(10,10,10,10));


        phoneContainer = new HBox();
        phoneContainer.getChildren().addAll(lblPhoneNumber,phoneField);
        phoneContainer.setPadding(new Insets(10,10,10,10));

        itemContainer = new HBox();
        itemContainer.getChildren().addAll(lblItem,itemField);
        itemContainer.setPadding(new Insets(10,10,10,10));

        usernameContainer.getChildren().addAll(lbluser,usernameField);
        usernameContainer.setPadding(new Insets(10,10,10,10));
        passwordContainer.getChildren().addAll(lblpassword,passwordField);
        passwordContainer.setPadding(new Insets(10,10,10,10));
        yesButton = new Button(CREATE);
        cancelButton = new Button(CANCEL);

        EventHandler<ActionEvent> yesNoCancelHandler = event -> {
            ProviderCreateSingleton.this.selection = ((Button) event.getSource()).getText();
            ProviderCreateSingleton.this.hide();


            if(((Button)event.getSource()).getText().equals("Create")) {
                username = usernameField.getText();
                password = passwordField.getText();
                name = nameField.getText();
                phone = phoneField.getText();
                itemId = Integer.valueOf((String)itemField.getValue());

                usernameField.clear();
                passwordField.clear();
                nameField.clear();
                phoneField.clear();

                Connection connection = null;
                try {
                    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false","root","li970623");
                    Statement statement =  connection.createStatement();

                    String query = "INSERT INTO Provider (Id,Password,ItemId,PhoneNumber,Name) VALUES (\""+username+"\",\""+password+"\","+itemId+","+phone+",\""+name+"\");";
                    System.out.println(query);
                    statement.executeUpdate(query);
                    if(connection != null){
                        System.out.println("Connection established");
                    }else System.out.println("Connection is not established");
                }catch (Exception e){
                    e.printStackTrace();
                }




                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        };

        yesButton.setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(yesNoCancelHandler);

        HBox buttonContainer = new HBox();
        buttonContainer.getChildren().addAll(yesButton,cancelButton);

        VBox infoField = new VBox();
        infoField.getChildren().addAll(usernameContainer,passwordContainer,nameContainer,phoneContainer,itemContainer);

        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().addAll(messageLabel,infoField,buttonContainer);
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);

    }
    public void show(String title, String message){
        setTitle(title);
        messageLabel.setText(message);
        showAndWait();
    }
}