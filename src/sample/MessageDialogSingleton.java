package sample;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;



public class MessageDialogSingleton extends Stage {

    private static MessageDialogSingleton singleton = null;

    private Label messageLabel;

    private MessageDialogSingleton() {
    }


    public static MessageDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new MessageDialogSingleton();
        return singleton;
    }


    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();

        Button closeButton = new Button("OK");
        closeButton.setOnAction(e -> this.close());

        VBox messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(closeButton);

        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);

        Scene messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }


    public void show(String title, String message) {
        setTitle(title); // set the dialog title
        messageLabel.setText(message); // message displayed to the user
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }



}
